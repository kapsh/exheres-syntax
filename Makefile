DATE = $(shell date +%Y%m%d)

all: exheres-syntax-$(DATE).tar.bz2

clean:
	@echo "Cleaning ..."
	@rm -rf *.tar.bz2 *.tar || true

exheres-syntax-$(DATE).tar.bz2:
	@echo "Generating $@ ..."
	@git archive --prefix=exheres-syntax-$(DATE)/ HEAD | tar --delete exheres-syntax-$(DATE)/.gitignore exheres-syntax-$(DATE)/Makefile | bzip2 -c > $@

upload: exheres-syntax-$(DATE).tar.bz2
	@echo "Uploading $< ..."
	scp $< dev.exherbo.org:/srv/www/dev.exherbo.org/distfiles/exheres-syntax/

.default: all

.phony: clean upload
